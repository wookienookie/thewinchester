Action skills
 - Action skills have been added for all weapons and tools. The more you use the items the better you become at using, crafting and repairing them. 
 - Skills for each item type go from 1 - 100. 
 - The higher your AS the better quality items you can make (up to a maximum quality of 901)
 - The higher your AS the less quality that is lost when an item is repaired
 - Books that help increase you skills can be found in loot

Item Quality 1 - 1000
 - All tools and weapons now go upto a maximum quality of 1000
 - There are now 10 levels instead of the standard 6
 - Quality of crafted items is tied to the relevant AS, the higher the AS the higher quality you can craft
 - Item quality degrades each time you repair them, starting with a lose of 100 quality per repair. This lose goes down as your relevant AS increases down to a minimum lose of 10 per repair (tools and weapons cannot last forever!).

Biome Zombie Spawning (i.e. Zombies that spawn outside POIs)
 - Zombies now have a chance to repawn quicker (sometimes much quicker!). See game option below
 - Cities are much more dangerous now and the more POIs around the more Zombies that will spawn. See game option below

Game Options (These settings are set at the game level and hence are set for all players)
 - Max Zombies - This is the maximum number of zombies that can spawn at any given time
 - City Zombie Multipier - Sets how many zombies spawn in cities. This is a multipler so the higher the number the more zombies that may spawn.
 - Biome Respawn Delay - This new setting sets how often biome zombies repawn. The lower the number the more often biome zombies may spawn.
 - Enable/Disable Head Shot Only mode
 - Enable/Disable Zombie Rage
 - Wandering Horde Frequency - Sets a range of how often wandering hordes spawn. i.e. 2 - 14 hours means a wandering horde will spawn much quick than normal
 - Wandering Horde Multiplier - Sets how many zombies spawn in wandering hordes. This is a multipler so the higher the number the more zombies that will spawn.

Video Options (These settings are set on a per player basis and each player can change their own values)
 - Show/Hide Crosshair
 - Show/Hide Activation Text
 - Show/Hide On Screen Sprites
 - Show/Hide Zombie Health Bar
 - Show/Hide XP Pop Up
 - Show/Hide Electrical Wires
 - Set Crosshair Color

Bigger backpack with custom stash logic
 - When moving items from your backpack to containers there are now 2 new buttons
 - Stash matching - This will automatically move items from your backpack to the open container if it matches an item already in the container.
 - Stash all - This will automatically move all items from your backpack into the open container.
 - X locked - This feature allows users to 'lock' some cells in your backpack so they will never automatically move items in these cells. To lock a cell hover over the cell and press the X button, the cell border will go green indicating it is now locked. To unlock a locked cell just hover and press X again.

Airdrops are now dangerous but dont wander too far from them if you want to get the loot!

Increased number of trader quests per tier
 - Each trader tier now opens after 10 completed quests (up from the standard 7 quests per tier)

Trader Prices
 - Traders have been too nice for too long! Purchase prices are now 3 times the normal price

Tier 5 quest rewards have a chance to now include a new book that grants the user 1 skill point

Slower XP gain
 - XP is gained at a slightly slower rate, youll get plenty of XP from killing all the zombies you encounter

Vehicles now trigger pressure plates
 - You no longer need to get off you bike or car to activate any pressure plates.

Vehicles disabled during bloodmoons
 - Mysteriously all vehicles (except for your trusty bicycle) no longer work during bloodmoons

Workstation smelting timers show total times and not increments
 - Stacks of materials in the forge now show the total smelting time! 
